#pragma once

#include <pds/point.hpp>

#include <algorithm>
#include <cstdint>
#include <math.h>
#include <optional>
#include <random>
#include <tuple>
#include <vector>

namespace pds {

template <typename T> class sampler {
public:
  using point_type = pds::point<T>;

  using cell_type = std::tuple<bool, std::size_t>;
  using row_type = std::vector<cell_type>;
  using grid_type = std::vector<row_type>;

  sampler(sampler<T> const &other)
      : sampler{other.extents(), other.minimum_distance(), other.limit()} {
    // NOTE: This does not copy the initial sample if one was provided,
    // nor the points. Only the "configuration".
  }

  sampler(T const extents, T const minimum_distance, std::size_t const limit)
      : _extents{extents}, _minimum_distance{minimum_distance},
        _squared_minimum_distance{minimum_distance * minimum_distance},
        _limit{limit},
        _cell_size{minimum_distance / static_cast<T>(std::sqrt(2))}, _grid{},
        _active_list{}, random_engine{random_device()}, urd{0.0, extents} {}

  sampler(T const extents, T const minimum_distance, std::size_t const limit,
          point_type const &initial_sample)
      : sampler(extents, minimum_distance, limit) {
    point_type const clamped{
        std::clamp(initial_sample.x, static_cast<T>(0), extents),
        std::clamp(initial_sample.y, static_cast<T>(0), extents)};
    initialize_grid();
    insert_sample(clamped);
  }

  sampler<T> &operator=(sampler<T> const &other) {
    // NOTE: This does not copy the initial sample if one was provided,
    // nor the points. Only the "configuration".
    _extents = other.extents();
    _minimum_distance = other.minimum_distance();
    _squared_minimum_distance = _minimum_distance * _minimum_distance;
    _limit = other.limit();
    _cell_size = (_minimum_distance / static_cast<T>(std::sqrt(2)));

    urd = std::move(std::uniform_real_distribution<T>{0.0, _extents});

    reset();

    return *this;
  }

  T extents() const { return _extents; }
  T minimum_distance() const { return _minimum_distance; }
  T squared_minimum_distance() const { return _squared_minimum_distance; }
  std::size_t limit() const { return _limit; }
  std::size_t dimensions() const { return 2; }
  T cell_size() const { return _cell_size; }

  std::optional<point<T>> next() {
    if (_points.size() == 0) {
      initialize_grid();

      point_type const sample{urd(random_device), urd(random_device)};
      return std::optional<point<T>>{sample};
    }

    return try_generate_new_sample();
  }

  void finish() {
    if (_points.size() == 0) {
      do_sampling();
      return;
    }

    while (!_active_list.empty()) {
      try_generate_new_sample();
    }
  }

  void reset() {
    _points.clear();
    _active_list.clear();
    _grid.clear();
  }

  void reset_and_finish() {
    reset();
    do_sampling();
  }

  std::vector<point_type> const &points() const { return _points; }

private:
  void do_sampling() {
    // Step 0: Initialize an n dimensional _grid. Cell size is bound by
    //         minimum_distance / (sqrt(dimensions))
    initialize_grid();

    // Step 1: Select initial sample randomly. Insert it into the _grid and
    //         initialize the "active list" with the index of the initial
    //         sample (0).
    point_type const initial_sample{urd(random_device), urd(random_device)};
    insert_sample(initial_sample);

    // Step 2: While the active list is not empty, choose a random index
    //         from it(say i).Generate up to k points chosen uniformly from
    //         the spherical annulus between radius r and 2r around x i.For
    //         each point in turn, check if it is within distance r of
    //         existing samples( using the background _grid to only test
    //         nearby samples). If a point is adequately far from existing
    //         samples, emit it as the next sample and add it to the active
    //         list.If after k attempts no such point is found, instead remove
    //         i from the active list.
    while (!_active_list.empty()) {
      try_generate_new_sample();
    }
  }

  void initialize_grid() {
    // TODO: ceil on integral
    std::size_t const grid_size = (_extents / _cell_size) + 1;
    for (size_t y = 0; y < grid_size; ++y) {
      row_type row{};
      for (size_t x = 0; x < grid_size; ++x) {
        row.push_back(cell_type{false, 0});
      }
      _grid.push_back(row);
    }
  }

  void insert_sample(point_type const &sample) {
    _points.push_back(sample);

    auto const grid_coord = get_grid_coord(sample, _cell_size);

    _grid[grid_coord.y][grid_coord.x] = cell_type{true, _points.size() - 1};
    _active_list.push_back(_points.size() - 1);
  }

  std::optional<point<T>> try_generate_new_sample() {
    if (_active_list.empty()) {
      return std::nullopt;
    }

    auto const indicies = find_random_origin_from_active_list();

    std::size_t const active_list_index{std::get<0>(indicies)};

    auto const &origin = _points[std::get<1>(indicies)];

    bool found_new_sample = false;
    point_type new_sample{};
    for (size_t tries = 0; tries < _limit; ++tries) {
      new_sample = generate_offset_sample_from(origin);

      bool const ok = check_neighbors(new_sample);

      if (ok) {
        insert_sample(new_sample);
        found_new_sample = true;
        break;
      }
    }

    if (!found_new_sample) {
      _active_list.erase(_active_list.begin() + active_list_index);
    }

    return found_new_sample ? std::optional<point<T>>{new_sample}
                            : std::nullopt;
  }

  point_type generate_offset_sample_from(point_type const &origin) {
    T const offset_distance_factor{dist_dist(random_device)};
    T const offset_distance{_minimum_distance * offset_distance_factor};
    T const rad{rad_dist(random_device)};

    point_type const offset{std::cos(rad) * offset_distance,
                            std::sin(rad) * offset_distance};

    // TODO: own clamp to avoid c++17 and extract clamp function (initial
    // sample)
    T const nsx{std::clamp(origin.x + offset.x, static_cast<T>(0), _extents)};
    T const nsy{std::clamp(origin.y + offset.y, static_cast<T>(0), _extents)};
    return point_type{nsx, nsy};
  }

  bool check_neighbors(point_type const &sample) {
    bool ok{true};

    for (size_t i = 0; i < _points.size(); ++i) {
      auto const &other = _points[i];
      auto const distance = sample.distance_to(other);

      if (distance < _minimum_distance) {
        ok = false;
        break;
      }
    }

    return ok;
  }

  std::tuple<std::size_t, std::size_t> find_random_origin_from_active_list() {
    std::uniform_int_distribution<std::size_t> uid{0, _active_list.size() - 1};
    // TODO: might not be needed
    std::size_t const active_list_index{uid(random_device)};

    std::size_t const points_index =
        _active_list.size() == 1 ? 0 : _active_list[active_list_index];

    return std::tuple<std::size_t, std::size_t>{active_list_index,
                                                points_index};
  }

  point<std::size_t> get_grid_coord(point_type const &sample,
                                    T const cell_size) const {
    std::size_t const grid_x{static_cast<std::size_t>(sample.x / cell_size)};
    std::size_t const grid_y{static_cast<std::size_t>(sample.y / cell_size)};

    return point<std::size_t>{grid_x, grid_y};
  }

  std::vector<point_type> _points;
  grid_type _grid;
  std::vector<std::size_t> _active_list;

  // 2 * PI = 6.2831, for a full circle
  std::random_device random_device;
  std::mt19937 random_engine;
  std::uniform_real_distribution<T> urd;

  std::uniform_real_distribution<T> rad_dist{0, static_cast<T>(6.2831)};
  std::uniform_real_distribution<T> dist_dist{static_cast<T>(1.0),
                                              static_cast<T>(2.0)};

  T _extents{};
  T _minimum_distance{};
  T _squared_minimum_distance{};
  std::size_t _limit{};
  T _cell_size{};
};

} // namespace pds
