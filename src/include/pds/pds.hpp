#pragma once

#include <pds/point.hpp>
#include <pds/sampler.hpp>

#include <cstdint>
#include <vector>

namespace pds {

template <typename T>
std::vector<pds::point<T>> get_points(T const extents, T const minimum_distance,
                                      std::size_t limit = 30) {
  pds::sampler<T> sampler{extents, minimum_distance, limit};
  sampler.finish();
  return sampler.points();
}

} // namespace pds
