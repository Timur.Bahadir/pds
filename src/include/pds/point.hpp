#pragma once

#include <cfloat>
#include <cmath>
#include <iostream>

namespace pds {

namespace {

template <typename T> constexpr T get_epsilon() { return 0; }
template <> constexpr float get_epsilon() { return FLT_EPSILON; }
template <> constexpr double get_epsilon() { return DBL_EPSILON; }
template <> constexpr long double get_epsilon() { return LDBL_EPSILON; }

// http://realtimecollisiondetection.net/blog/?p=89
template <typename T> bool compare_real(T const lhs, T const rhs) {
  return std::abs(lhs - rhs) <= get_epsilon<T>() * std::max({1.0f, std::abs(lhs), std::abs(rhs)});
}

} // namespace

template <typename T> class point {
public:
  point() : point(0, 0) {}
  point(T const x, T const y) : x{x}, y{y} {}

  T x{};
  T y{};

  T const squared_distance_to(point<T> const &other) const {
    auto const distance = other - *this;
    return (distance.x * distance.x) + (distance.y * distance.y);
  }

  T const distance_to(point<T> const &other) const {
    return std::sqrt(squared_distance_to(other));
  }

  bool operator==(const point<T> &rhs) const {
    // TODO: simplyfy for integral
    bool const eqx = compare_real(x, rhs.x);
    bool const eqy = compare_real(y, rhs.y);
    return eqx && eqy;
  }

  bool operator!=(const point<T> &rhs) const { return !(*this == rhs); }

  point<T> &operator+=(point<T> const &rhs) {
    x += rhs.x;
    y += rhs.y;
    return *this;
  }

  point<T> &operator-=(point<T> const &rhs) {
    x -= rhs.x;
    y -= rhs.y;
    return *this;
  }

  point<T> &operator*=(T const factor) {
    x *= factor;
    y *= factor;
    return *this;
  }

  friend point<T> operator+(point<T> lhs, point<T> const &rhs) {
    lhs += rhs;
    return lhs;
  }

  friend point<T> operator-(point<T> lhs, point<T> const &rhs) {
    lhs -= rhs;
    return lhs;
  }

  friend point<T> operator*(point<T> lhs, T factor) {
    lhs *= factor;
    return lhs;
  }

  friend std::ostream &operator<<(std::ostream &os, point<T> const &p) {
    os << "(" << p.x << ", " << p.y << ")";
    return os;
  }
};

} // namespace pds
