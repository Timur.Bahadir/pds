#include <iostream>
#include <string>

#include <pds/pds.hpp>

template <typename T>
void spawn_creature_at(std::string const &name, pds::point<T> const &point) {
  std::cout << "Spawning " << name << " at: " << point << std::endl;
}

int main() {
  // extents: is the square size of the total domain size in which the
  //          samples are contrained in
  // minimum_distance: the minimum distance between samples
  // limit: how many attempts are made for each new sample
  float const extents{50.0f};
  float const minimum_distance{30.0f};
  std::size_t const limit{25};

  pds::sampler<float> sampler{extents, minimum_distance, limit};
  sampler.finish();

  for (auto &&point : sampler.points()) {
    spawn_creature_at("Imp", point);
  }

  // If you want to get another set of points with the same initial parameters
  // you can rerun the sampling process
  sampler.reset_and_finish();
  for (auto &&point : sampler.points()) {
    spawn_creature_at("Goblin", point);
  }

  // There is also a shorthand to just get a one time use list of points
  for (auto &&point : pds::get_points(10.0, 6.0, 10)) {
    spawn_creature_at("Orc", point);
  }

  // If desired you can also do the sampling one by one and if desired you can
  // also specify a initial sample location
  using sampler_type = pds::sampler<float>;
  sampler_type isampler{extents, minimum_distance, limit,
                        sampler_type::point_type{20.0f, 20.0f}};

  // sampler::next() returns an std::optional<sampler_type::point_type>
  auto next = isampler.next();
  while (next != std::nullopt) {
    spawn_creature_at("Ogre", next.value());
    next = isampler.next();
  }
}
