#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

#include <cstdint>
#include <vector>

#include "PDSTestFixture.hpp"

TEST_SUITE("sampler test suite") {
  TEST_CASE_FIXTURE(pds::PointsFixture, "Testing the pds algorithm") {
    float const extents{100.0};
    float const minimum_distance{8.0};
    float const squared_minimum_distance{minimum_distance * minimum_distance};
    std::size_t const limit{15};

    SUBCASE("basic usage") {
      pds::sampler<float> sampler{extents, minimum_distance, limit};
      sampler.finish();

      REQUIRE(!sampler.points().empty());

      REQUIRE(validate_params(sampler, extents, minimum_distance, limit));

      REQUIRE(validate_points(sampler.points(), extents, minimum_distance));
    }

    SUBCASE("initial sample") {
      using sampler_type = pds::sampler<float>;

      sampler_type::point_type const initial_point{25.0f, 25.0f};

      pds::sampler<float> sampler{extents, minimum_distance, limit,
                                  initial_point};

      REQUIRE(sampler.points().size() == 1);

      sampler.finish();

      REQUIRE(validate_params(sampler, extents, minimum_distance, limit));

      REQUIRE(validate_points(sampler.points(), extents, minimum_distance));

      REQUIRE(sampler.points().front() == initial_point);
    }

    SUBCASE("initial sample iterative") {
      using sampler_type = pds::sampler<float>;

      sampler_type::point_type const initial_point{25.0f, 25.0f};

      pds::sampler<float> sampler{extents, minimum_distance, limit,
                                  initial_point};

      REQUIRE(sampler.points().size() == 1);

      sampler.next();

      REQUIRE(sampler.points().size() == 2);

      sampler.next();

      REQUIRE(sampler.points().size() == 3);

      sampler.finish();

      REQUIRE(validate_params(sampler, extents, minimum_distance, limit));

      REQUIRE(validate_points(sampler.points(), extents, minimum_distance));

      REQUIRE(sampler.points().front() == initial_point);
    }

    SUBCASE("use next return") {
      using sampler_type = pds::sampler<float>;

      sampler_type::point_type const initial_point{25.0f, 25.0f};

      pds::sampler<float> sampler{extents, minimum_distance, limit,
                                  initial_point};

      auto const point = sampler.next();
      REQUIRE(point.has_value());

      REQUIRE(!sampler.points().empty());

      REQUIRE(validate_params(sampler, extents, minimum_distance, limit));

      REQUIRE(validate_points(sampler.points(), extents, minimum_distance));
    }

    SUBCASE("unable to sample") {
      pds::sampler<float> sampler{extents, minimum_distance, 100};
      sampler.finish();

      REQUIRE(!sampler.points().empty());

      REQUIRE(validate_params(sampler, extents, minimum_distance, 100));

      REQUIRE(validate_points(sampler.points(), extents, minimum_distance));

      auto const result = sampler.next();

      // TODO: this might be flaky
      REQUIRE_FALSE(result.has_value());
    }

    SUBCASE("reset sampler") {
      pds::sampler<float> sampler{extents, minimum_distance, limit};
      sampler.finish();

      REQUIRE(!sampler.points().empty());

      REQUIRE(validate_params(sampler, extents, minimum_distance, limit));

      REQUIRE(validate_points(sampler.points(), extents, minimum_distance));

      sampler.reset();

      REQUIRE(sampler.points().empty());

      REQUIRE(validate_params(sampler, extents, minimum_distance, limit));
    }

    SUBCASE("reset and finish sampler") {
      pds::sampler<float> sampler{extents, minimum_distance, limit};
      sampler.finish();

      REQUIRE(!sampler.points().empty());

      REQUIRE(validate_params(sampler, extents, minimum_distance, limit));

      REQUIRE(validate_points(sampler.points(), extents, minimum_distance));

      sampler.reset_and_finish();
      // TODO: maybe check they're not identical?

      REQUIRE(!sampler.points().empty());
      REQUIRE(validate_params(sampler, extents, minimum_distance, limit));

      REQUIRE(validate_points(sampler.points(), extents, minimum_distance));
    }

    SUBCASE("copy assignement") {
      using sampler_type = pds::sampler<float>;

      sampler_type::point_type const initial_point{25.0f, 25.0f};

      sampler_type sampler{extents, minimum_distance, limit, initial_point};
      sampler.finish();

      sampler_type other{1.0f, 1.0f, 10};
      other = sampler;
      other.finish();

      // This has a ridiculously tiny chance of being flaky
      REQUIRE(other.points().front() != initial_point);

      REQUIRE(validate_params(other, extents, minimum_distance, limit));

      REQUIRE(validate_points(other.points(), extents, minimum_distance));
    }

    SUBCASE("copy constructor") {
      using sampler_type = pds::sampler<float>;

      sampler_type::point_type const initial_point{25.0f, 25.0f};

      sampler_type sampler{extents, minimum_distance, limit, initial_point};
      sampler.finish();

      sampler_type other{sampler};
      other.finish();

      // This has a ridiculously tiny chance of being flaky
      REQUIRE(other.points().front() != initial_point);

      REQUIRE(validate_params(other, extents, minimum_distance, limit));

      REQUIRE(validate_points(other.points(), extents, minimum_distance));
    }
  }
}
