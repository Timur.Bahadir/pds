#pragma once

#include "doctest.h"

#include <cstdint>
#include <vector>

#include <pds/pds.hpp>

namespace pds {

class PointsFixture {
protected:
  template <typename T>
  bool validate_params(pds::sampler<T> const &sampler, T const expected_extents,
                       T const expected_minimum_distance,
                       std::size_t const expected_limit) {
    return sampler.extents() == expected_extents &&
           sampler.minimum_distance() == expected_minimum_distance &&
           sampler.limit() == expected_limit;
  }

  template <typename T>
  bool validate_points(std::vector<pds::point<T>> const &points,
                       float const extents, float const minimum_distance) {
    for (size_t i = 0; i < points.size() - 1; ++i) {
      auto const &pa = points[i];

      bool const in_extents{pa.x <= extents && pa.y <= extents};
      REQUIRE(in_extents);

      for (size_t j = i + 1; j < points.size(); ++j) {
        auto const &pb = points[j];
        auto distance = pa.distance_to(pb);

        if (distance < minimum_distance) {
          return false;
        }
      }
    }

    auto const &last = points.back();
    bool const in_extents{last.x <= extents && last.y <= extents};
    REQUIRE(in_extents);

    return true;
  }
};

} // namespace pds
