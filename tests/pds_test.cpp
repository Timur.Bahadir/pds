#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

#include <cstdint>
#include <vector>

#include "PDSTestFixture.hpp"

TEST_SUITE("pds test suite") {
  TEST_CASE_FIXTURE(pds::PointsFixture, "Testing pds::get_points") {
    float const extents{50.0f};
    float const minimum_distance{15.0f};
    std::size_t const limit{25};

    auto const points = pds::get_points(extents, minimum_distance, limit);

    REQUIRE(points.size() > 0);

    bool const valid{validate_points(points, extents, minimum_distance)};
    REQUIRE(valid);
  }
}
