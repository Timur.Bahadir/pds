#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

#include <cstdint>

#include <pds/pds.hpp>

TEST_CASE("Testing the point class distance methods") {
  pds::point<float> point_a{3, 3};
  pds::point<float> point_b{5, 5};
  pds::point<float> point_c{point_a.x, point_a.y};

  REQUIRE(point_a.squared_distance_to(point_b) == doctest::Approx(8));
  REQUIRE(point_a.distance_to(point_b) == doctest::Approx(2.828427));

  REQUIRE(point_a.squared_distance_to(point_c) == doctest::Approx(0));
  REQUIRE(point_a.distance_to(point_c) == doctest::Approx(0));
}

TEST_CASE("Testing the point class operator overloads") {
  pds::point<float> point_a{3.0, 3.0};
  pds::point<float> point_b{5.0, 5.0};
  pds::point<float> point_c{point_a.x, point_a.y};

  REQUIRE_EQ(point_a == point_b, false);
  REQUIRE_EQ(point_a == point_c, true);
  REQUIRE_EQ(point_a != point_b, true);
  REQUIRE_EQ(point_a != point_c, false);

  pds::point<float> const point_ab = point_a + point_b;
  REQUIRE(point_ab == pds::point<float>{8.0, 8.0});

  pds::point<float> point_d{10.0, 10.0};
  point_d += point_a;
  REQUIRE(point_d == pds::point<float>{13.0, 13.0});

  pds::point<float> point_e{10.0, 10.0};
  point_e -= point_a;
  REQUIRE(point_e == pds::point<float>{7.0, 7.0});

  pds::point<float> point_f{10.0, 10.0};
  point_f *= 5.0;
  REQUIRE(point_f == pds::point<float>{50.0, 50.0});
}
