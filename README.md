# Poisson-Disc Sampling

A very basic C++ library for the **Poisson-Disc Sampling** algorithm.

## What is Poisson-Disc Sampling

Poisson-Disc Sampling generates a set of distributed across an area that
are at least a **minimum distace** apart.
A nice visual representation [can be found here](https://www.jasondavies.com/poisson-disc/).
The algrotihm used is based on [this paper](http://www.cs.ubc.ca/~rbridson/docs/bridson-siggraph07-poissondisk.pdf) by Robert Bridson.

## How to use

Include this lib in your project.

### Include it into your project

You have multiple ways of including this lib in your project.
Since it is header-only the setup should be quite easy.

#### Copy the headers manually

I will not help you with this...

#### CMake

As a git submodule. **Preferred**

```bash
git submodule add git@gitlab.com:Timur.Bahadir/pds.git
```

And then in your CMakeLists.txt

```cmake
add_subdirectory(pds)
...
target_link_library(<your_target> {PRIVATE|INTERFACE|PUBLIC} pds::pds_lib)
```

Alternatively you can use the FetchContent package.

```cmake
FetchContent_Declare(pds
  GIT_REPOSITORY git@gitlab.com:Timur.Bahadir/pds.git
)

FetchContent_MakeAvailable(pds)
...
target_link_library(<your_target> {PRIVATE|INTERFACE|PUBLIC} pds::pds_lib)
```

### API

You can check out the unit tests [here](tests/pds_test.cpp) for some more concrete
examples.

```c++
#include <pds/pds.hpp>
...
// extents: is the square size of the total domain size in which the
//          samples are contrained in
// minimum_distance: the minimum distance between samples
// limit: how many attempts are made for each new sample
float const extents{50.0f};
float const minimum_distance{30.0f};
std::size_t const limit{25};

// Creates a new sampler that caches the configuration and list of
// sampled points
pds::sampler<float> sampler{extents, minimum_distance, limit};
// The list of points is initially empty. The sampling can be (started and)
// finished by calling pds::sampler::finish()
sampler.finish();

// After calling finish the pds::sampler::points() list is populated
// and can be accessed
for (auto &&point : sampler.points()) {
  spawn_creature_at("Imp", point);
}

// If you want to get another set of points with the same initial parameters
// you can rerun the sampling process
sampler.reset_and_finish();
for (auto &&point : sampler.points()) {
  spawn_creature_at("Goblin", point);
}

// There is also a shorthand to just get a one time use list of points
for (auto &&point : pds::get_points(10.0, 6.0, 10)) {
  spawn_creature_at("Orc", point);
}

// If desired you can also do the sampling one by one and if desired you can
// also specify a initial sample location
using sampler_type = pds::sampler<float>;
sampler_type isampler{extents, minimum_distance, limit,
  sampler_type::point_type{20.0f, 20.0f}};

// sampler::next() returns an std::optional<sampler_type::point_type>
auto next = isampler.next();
while (next != std::nullopt) {
  spawn_creature_at("Ogre", next.value());
  next = isampler.next();
}
```

## Examples

![Example image of sampled poinits](misc/screenshots/example_1.png "Example image of sampled poinits")

Result of:

```c++
auto const points = pds::get_points(900.0f, 50.0f, 25);
```

"Rendered" in Godot.

## Building

```bash
# Configure
cmake -S . -B build
# Build
cmake --build build
```

Needs C++17 because of `std::optional` (and `std::clamp()`).

## TODO

- Build time checks if extents/minum_distance are valid? asserts? limit > 0
- documentation
- The whole grid thing...
- CICD
  - static analysis
